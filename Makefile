UID := $(shell id -u)
GID := $(shell id -g)
# Determine the system operator
OS := $(shell uname)
PROJECT_NAME := $(shell basename ${PWD})


define create-dirs
    @if [ ! -d "app/data" ]; then \
        echo "Creating data directory app/data"; \
        mkdir -p app/data; \
    fi
    @if [ ! -d "output" ]; then \
        echo "Creating output folder output"; \
        mkdir -p output; \
    fi
endef

define freeze_file
	git update-index --assume-unchanged $1
endef

define unfreeze_file
	git update-index --no-assume-unchanged $1
endef

check-dirs:
	$(create-dirs)


define build_cli
	rm -rf ./app/etl-cli/dist
	cd app/etl-cli && poetry build
	cp -r ./app/etl-cli/dist ./docker/otterdog_opensearch_etl/dist
endef

build: check-dirs
	$(call build_cli)
	docker-compose build --build-arg USER_ID=$(UID) --build-arg GROUP_ID=$(GID)
	rm -r ./docker/otterdog_opensearch_etl/dist

up: check-dirs
	docker-compose up -d


clean_compose:
	docker-compose stop
	docker-compose rm -f
	docker rmi -f otterdog-opensearch-etl otterdog-grafana
	docker volume remove $(PROJECT_NAME)_opensearch-data1
	docker volume remove $(PROJECT_NAME)_grafana_data
	rm -rf output/*


freeze_etl_env:
	$(call freeze_file,./docker/otterdog_opensearch_etl/env)

unfreeze_etl_env:
	$(call unfreeze_file,./docker/otterdog_opensearch_etl/env)

run_dashboard:
    ifeq ($(OS), Linux)
		@google-chrome --incognito http://localhost:5601
    endif

run_grafana:
    ifeq ($(OS), Linux)
		@google-chrome --incognito http://localhost:3000
    endif


update_project:
	git fetch
	git reset --keep origin/main

build_etl_package:
	$(call build_cli)

build_etl:
	$(call build_cli)
	cp -r ./app/etl-cli/dist ./docker/otterdog_opensearch_etl/dist
	docker-compose build --build-arg USER_ID=$(UID) --build-arg GROUP_ID=$(GID) otterdog-opensearch-etl
	rm -r ./docker/otterdog_opensearch_etl/dist

up_etl:
	docker-compose up -d otterdog-opensearch-etl

run_etl:
	docker run -it --rm --name otterdog_opensearch_etl -v ./scripts:/scripts -v ./app/data:/data -v ./output:/output otterdog_opensearch_etl:latest

shell_etl:
	docker-compose exec otterdog-opensearch-etl /bin/bash

shell_grafana:
	docker-compose exec otterdog-grafana /bin/bash


logs_etl:
	docker-compose logs -f otterdog-opensearch-etl

shell_node1:
	docker-compose exec opensearch-node1 /bin/bash

logs_node1:
	docker-compose logs -f opensearch-node1

logs:
	docker-compose logs -f

shell_dashboards:
	docker-compose run opensearch-dashboards /bin/bash

clean_etl:
	docker-compose stop otterdog-opensearch-etl
	docker-compose rm -f otterdog-opensearch-etl
	docker rmi -f otterdog-opensearch-etl


up_grafana:
	docker-compose up -d otterdog-grafana

build_grafana:
	docker-compose build otterdog-grafana

clean_grafana:
	docker-compose stop otterdog-grafana
	docker-compose rm -f otterdog-grafana
	docker rmi -f otterdog-grafana
	docker volume remove otterdog-opensearch_grafana_data


.PHONY: up clean_compose clean_grafana build_etl run_etl clean_etl shell_etl build check-dirs run_dashboard shell_node1 shell_dashboards logs_node1 logs_etl logs run_grafana build_etl build_etl_package
