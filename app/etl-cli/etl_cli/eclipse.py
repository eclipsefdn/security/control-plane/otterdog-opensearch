import requests


class EclipseAPI:
    def __init__(self):
        self.base_url = "https://projects.eclipse.org/api/projects"

    def get_projects(self):
        page = 1
        while True:
            response = requests.get(self.base_url, params={"pagesize": 100, "page": page})
            if response.status_code == 200:
                projects = response.json()
                if not projects:
                    break
                for project in projects:
                    yield project
                page += 1
            else:
                response.raise_for_status()