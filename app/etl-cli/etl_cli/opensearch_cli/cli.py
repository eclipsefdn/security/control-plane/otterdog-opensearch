import json
import urllib.parse
import os
import sys
from datetime import datetime
from opensearchpy import helpers, OpenSearch


def console_log(type_msg, msg):
    timestamp = datetime.now()
    print(f"{timestamp.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]} {type_msg}: {msg}")
    pass


class OpenSearchETL:
    
    def __init__(self, os_username: str = "admin", os_password: str = "admin", 
                 os_hostname: str = "localhost", os_port: str = "9200", os_index_name: str = "") -> None:
        self.os_username = urllib.parse.quote_plus(os_username)
        self.os_password = urllib.parse.quote_plus(os_password)
        self.os_hostanme = os_hostname
        self.os_port = os_port
        self.os_index_name = os_index_name
        self.service_uri = f"https://{os_username}:{os_password}@{os_hostname}:{os_port}"
        self.os_client = OpenSearch(hosts=self.service_uri, ssl_enable=True,verify_certs=False)

    def insert_json_file(self, json_file_path: str):
        """
        Pushing a json documents upon OpenSearch using OpenSearch bulk helper

        Args:
            json_file_path (str): A json file full path
            _id (str): An OpenSearch document id
        """
        if self.os_client:
            console_log("INFO",f"Connected to server: {self.os_hostanme}")
            try:
                helpers.bulk(self.os_client, self.load_data(json_file_path))
            except Exception as ex:
                console_log("ERROR",f"connecting to {self.service_uri} msg {ex}")

    def load_data(self, json_file_path: str):
        """
        Load a json file into an opensearch for a given json_file_path using as id epoch timestamp.

        Args:
            json_file_path (str): A json file full path
        """
        timestamp = datetime.now()
        print(f"{timestamp.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]} Processing file {json_file_path}")
        with open(json_file_path, "r") as json_file:
            json_data = json.load(json_file)
            ### TRANSFORMATION SECTION. Please use a decorator
            yield {'_index': f'{self.os_index_name}', '_id': timestamp.timestamp(), '_source': json_data}

    def load_doc(self, document: dict, id: str):
        try:
            result = self.os_client.index(index=self.os_index_name, body=document, id=id, refresh=True)
            console_log("INFO", f"Document uploaded, more information: {result}")
        except Exception as e:
            console_log("ERROR!!", f"Something unexpected has happened, more information: {e}")

    def updating_mapping_propperties(self):
        """
        Modify mappings for index given at time that class is built.

        Args:
            json_file_path (str): A json file full path
        """
        try:
            properties = self.os_client.indices.get(self.os_index_name)
            ### This method by defualt is updating the mapping by itself
            # Please take de temaplte below to costumize using a decorator 
            # properties[os_index_name]['mappings']['properties']['org_github_id']['fielddata']=True
            self.os_client.indices.put_mapping(index=self.os_index_name,body=properties[self.os_index_name]['mappings'])
            console_log("INFO", f"Mapping propperties updated")
        except Exception as e:
            console_log("ERROR", f"An expected error happened when {self.os_index_name} as updated")
            console_log("ERROR",e)

    ## TO-DO maneging the scroll
    def query(self, dsl_query: dict):
        return self.os_client.search(index=self.os_index_name, body=dsl_query)
        