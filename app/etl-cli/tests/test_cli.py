import unittest

from datetime import datetime
import os

from etl_cli.opensearch_cli.cli import OpenSearchETL



class TestOpenSearchETL_cli(unittest.TestCase):

    def test_insert_document(self):
        index_timestamp = datetime.now().strftime('%Y-%m-%d')
        json_file = f"{os.path.abspath(os.path.curdir)}/tests/resource_test/adoptium.json"
        os_index_name = f'test-{index_timestamp}'
        cli = OpenSearchETL(os_index_name=os_index_name)

        cli.insert_json_file(json_file)
        ### TO-DO to compare doc upload with file
        self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()