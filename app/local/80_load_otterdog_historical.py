# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************


import json
import os
import sys
from datetime import datetime
from datetime import timedelta
from etl_cli.opensearch_cli.cli import OpenSearchETL
from etl_cli.opensearch_cli.cli import console_log
from collections import Counter


### TO-DO Converting this script in a class where load_data could be decorated

OS_INDEX_COMMITS = 'otterdog-commits'
OS_INDEX_KPIS = 'otterdog-kpis'
OS_HOSTNAME = os.environ["OS_HOSTNAME"]


DATA_PATH = os.environ["DATA_PATH"]

LOCK_UPLOAD_HISTORICAL_DATA_FILE_PATH=f'{os.path.expanduser("~")}/.upload_historical_data.lock'

def load_variables():
    working_dir=os.path.dirname(os.path.abspath(__file__))
    config_file = f"{working_dir}/.config_{os.path.basename(__file__).split('.py')[0]}.json"
    try:
        with open(config_file, 'r') as config_data:
            console_log("INFO", f"Loading configuration file: {config_file}")
            global_variables = json.load(config_data)
            ## parsing files
            ## TO-DO parsing as well as CSV files
            for k,v in global_variables.items():
                if isinstance(v, dict):
                    if 'json' in v.values():
                        try:
                            console_log("INFO", f"Parsing varible {k}, definition file type {v['file_type']} getting definition from: {v['file_definition_path']}")
                            file_definition=f"{working_dir}/{v['file_definition_path']}"
                            with open(file_definition, 'r') as data:
                                global_variables[k]=json.load(data)
                        except FileNotFoundError:
                            console_log("ERROR",f"file: {file_definition} doesn't exists.")
        globals().update(global_variables)
    except json.JSONDecodeError as e:
        console_log("ERROR", f"file: {config_file} is not a valid JSON format. Error {e}")
        sys.exit(1)
    except FileNotFoundError:
        console_log("ERROR",f"file: {config_file} doesn't exists. Please check the file has the right format or follow name convetion .config_PYTHON_SCRIPT_NAME.json")
        sys.exit(1)



def get_github_id_kpis_list(list_doc: list, timestamp: int) -> list:
    """
        Getting a list of github_id with their kpis for given day from a result from a query run on OpenSearch

        Args:
            list_doc (list): List of documents from a query run on OpenSearch
            timestamp (int): Epoch timestamp
    """
    list_kpis = []
    def exists_github_id(github_id:str):
        for k in list_kpis:
            if github_id in k["github_id"]:
                return k
        return None
    doc_template = {
        "github_id": "",
        "@timestamp": 0,
        "sha_commit": "",
        "total_repositories": 0,
        "default_branch_protection": {
            "protected": 0,
            "not_protected": 0
        }, "secrcontrol_github_idet_scanning":{
            "enabled": 0,
            "disabled": 0
        }
    }
    for d in list_doc:
        result = exists_github_id(d["_source"]["github_id"])
        if result:
            console_log("WARNING!!",f"Already added doc: {result} removing from list_kpis queue and getting the latest one")
            list_kpis.remove(result)
        doc = doc_template.copy()
        doc["github_id"] = d["_source"]["github_id"]
        doc["@timestamp"] = timestamp
        doc["sha_commit"] = d["_source"]["sha_commit"]
        doc["total_repositories"] = len(d["_source"]["repositories"])
        doc["default_branch_protection"] = dict(Counter([k["default_branch_protection"] for k in d["_source"]["repositories"]]))
        doc["secret_scanning"] = dict(Counter([k["secret_scanning"] for k in d["_source"]["repositories"]]))
        list_kpis.append(doc)
    return list_kpis

def get_kpis_doc(list_kpis_doc: list, timestamp: int) -> dict:
    """
        Getting a kpis document from a list of kpis doc from a given timestamp produciendo a dict with a total for each kpi

        Args:
            list_kpis_doc (list): List of documents from a query run on OpenSearch
            timestamp (int): Epoch timestamp
    """
    doc_template = {
        "@timestamp": 0,
        "total_github_id": [],
        "total_repositories": [],
        "default_branch_protection": Counter(), 
        "secret_scanning": Counter()
    }
    doc = doc_template.copy()
    for d in list_kpis_doc:
        doc["total_github_id"].append(d["github_id"]) 
        doc["@timestamp"] = timestamp
        doc["total_repositories"].append(d["total_repositories"])
        doc["default_branch_protection"].update(d["default_branch_protection"])
        doc["secret_scanning"].update(d["secret_scanning"]) 
    result = {
        "@timestamp": datetime.utcfromtimestamp(timestamp).strftime("%Y-%m-%dT%H:%M:%S"),
        "total_github_id": len(doc["total_github_id"]),
        "total_repositories": sum(doc["total_repositories"]),
        "default_branch_protection": dict(doc["default_branch_protection"]),
        "secret_scanning": dict(doc["secret_scanning"])
    }
    result["default_branch_protection"].update( { f'perc_{k}': round(v/sum(result['default_branch_protection'].values()),4) for k,v in result['default_branch_protection'].items() })
    result["secret_scanning"].update( { f'perc_{k}': round(v/sum(result['secret_scanning'].values()),4) for k,v in result['secret_scanning'].items() })
    return result




def build_historical_data():
    ### TO-DO checking if OS_INDEX_NAME_KPIS exists else finish with exit(0)
    cli = OpenSearchETL(os_hostname=OS_HOSTNAME, os_index_name=OS_INDEX_COMMITS)
    load_variables()
    console_log("INFO", f"{QUERY_TIMESTAMP_WITH_DOCS}")
    timeline = sorted(cli.query(QUERY_TIMESTAMP_WITH_DOCS)['aggregations']['TIMESTAMP_WITH_DOCS']['buckets'], key = lambda x : x['key'])
    console_log("INFO", f"timeline: {timeline}")

    ### Controlling comulative
    comulative = []
    for t in timeline[:-1]:
        start = (t['key'] // 1000)
        end = int((datetime.utcfromtimestamp(start) + timedelta(days=1)).timestamp())
        start_as_string = t['key_as_string']
        end_as_string = datetime.utcfromtimestamp(end).strftime('%Y-%m-%d')
        doc_count= t['doc_count']
        console_log("INFO",f"Filtering docoments between {start} ({start_as_string}) and {end} ({end_as_string}) timestamp with {t['doc_count']} docs")
        QUERY_DOCS_GIVEN_TIMESTAMP['query']['range']['@timestamp']['gte'] = start * 1000
        QUERY_DOCS_GIVEN_TIMESTAMP['query']['range']['@timestamp']['lte'] = end * 1000
        result = cli.query(QUERY_DOCS_GIVEN_TIMESTAMP)
        if doc_count == 0:
            try: 
                console_log("INFO", f"doc_count (QUERY_DOCS_GIVEN_TIMESTAMP) == 0 from {start} ({t['key_as_string']})")
                previous_timestamp_doc = int((datetime.utcfromtimestamp(start) - timedelta(days=1)).timestamp())
                console_log("INFO", f"Computing previous timestamp {previous_timestamp_doc} ({datetime.utcfromtimestamp(previous_timestamp_doc).strftime('%Y-%m-%d')})")
                cli_get_doc = OpenSearchETL(os_hostname=OS_HOSTNAME, os_index_name=OS_INDEX_KPIS)
                previous_kpi_doc = cli_get_doc.os_client.get(index=OS_INDEX_KPIS,id=previous_timestamp_doc)['_source']
                console_log("INFO", f"Got previous doc from: {previous_timestamp_doc} ({datetime.utcfromtimestamp(previous_timestamp_doc).strftime('%Y-%m-%d')}) previous_kpi_doc = {previous_kpi_doc}")
                ### Modifying previous document as current document for timestamp == start
                previous_kpi_doc["@timestamp"] = t['key_as_string']
                console_log("INFO", f"Previous doc modified to make current timestamp: {start} ({start_as_string}) and {end} ({end_as_string}), result: {previous_kpi_doc}")
                cli_get_doc.load_doc(previous_kpi_doc,start)
                console_log("INFO", f"Uploaded doc modified to make current timestamp: {start} ({start_as_string}) and {end} ({end_as_string}), result: {previous_kpi_doc}")
            except Exception as e:
                console_log("ERROR!!", f"An error retriving kpi doc with id: {previous_kpi_doc} for {start} ({start_as_string}) and {end} ({end_as_string}), msg: {e}")
        elif doc_count == result['hits']['total']['value']:
            try:
                console_log("INFO", f"doc_count (QUERY_DOCS_GIVEN_TIMESTAMP) == result['hits']['total']['value'] (QUERY_DOCS_GIVEN_TIMESTAMP) ==> {result['hits']['total']['value']} docs")
                r = get_github_id_kpis_list(result['hits']['hits'], start)
                console_log("INFO", f"{len(r)} docs between {start} and {end}: {r}")
                comulative_index = set(map(lambda x: x['github_id'], comulative))
                r_index = set(map(lambda x: x['github_id'], r))
                commun_elemets = comulative_index.intersection(r_index)
                if not commun_elemets:
                    ### Intersection empty thus adding
                    comulative += r
                else:
                    ## Updating comulative by removing commun elemts with (r) from the past
                    console_log("WARNING!!", f"Removing elements: {commun_elemets} ( {len(commun_elemets)} docs) from timestamp: {start} and adding elements: {r}")
                    comulative = list(filter(lambda x: not x['github_id'] in commun_elemets, comulative))
                    comulative += r
                comulative_result = get_kpis_doc(comulative,start)
                console_log("INFO", f"Num of docs: {len(comulative)} ====>> {comulative_result}")
                console_log("INFO", f"comulative result: {comulative_result}")
                cli_upload = OpenSearchETL(os_hostname=OS_HOSTNAME, os_index_name=OS_INDEX_KPIS)
                cli_upload.load_doc(comulative_result,start)
                console_log("INFO", f"Uploaded comulative doc for timestamp: {start} ({t['key_as_string']}), result: {comulative_result}")
            except Exception as e:
                console_log("ERROR!!", f"An error retriving kpi doc with id: {comulative_result}, msg: {e}")
        else:
            console_log("ERROR!!!", f"Condition doc_count ({doc_count}) == result['hits']['total']['value'] ({result['hits']['total']['value']}) not satisfied")



class OpenSearchETL_historical(OpenSearchETL):
    def load_data(self,json_file_path:str):
        # Load JSON data from file
        timestamp = datetime.now()
        print(f"{timestamp.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]} Processing file {json_file_path}")
        branch_patterns = lambda y: list(map( lambda x: x['pattern'],  y['branch_protection_rules']))
        with open(json_file_path, "r") as json_file:

            json_data = json.load(json_file)
            ### TRANSFORMATION SECTION
            ## unwind repositories list
            for d in json_data['repositories']:
                ### Adding github org data to a repository
                console_log("INFO",f"Analysing default_branch_protection for repo: {d['name']} from github_id: {json_data['github_id']}")
                d['default_branch_protection'] = 'protected' if d['default_branch'] in branch_patterns(d) else 'not_protected'
                console_log("INFO",f"State default_branch_protection = {d['default_branch_protection']} for repo: {d['name']} from github_id: {json_data['github_id']}")
                ### Defining a document index as github_id + _ + repo.name
            yield {'_index': f'{self.os_index_name}', '_id':f"{json_data['sha_commit']}_{json_data['github_id']}", '_source': json_data}



    def updating_mapping_propperties(self):
        try:
            properties = self.os_client.indices.get(self.os_index_name)
            properties[self.os_index_name]['mappings']['properties']['repositories']['properties']['default_branch_protection']['fielddata']=True
            properties[self.os_index_name]['mappings']['properties']['github_id']['fielddata']=True
            self.os_client.indices.put_mapping(index=self.os_index_name,body=properties[self.os_index_name]['mappings'])
            console_log("INFO", f"Mapping propperties updated")
        except Exception as e:
            console_log("ERROR", f"An expected error happened when {self.os_index_name} as updated")
            console_log("ERROR",e)

def uploading_hitorical_data():
    cli = OpenSearchETL_historical(os_hostname=OS_HOSTNAME, os_index_name=OS_INDEX_COMMITS)
    for filename in os.listdir(DATA_PATH):
        if filename.endswith(".json"):
            file_path = os.path.join(DATA_PATH,filename)
            cli.insert_json_file(file_path)
            console_log("INFO",f"Processed file: {filename}")
    cli.updating_mapping_propperties()
    with open(LOCK_UPLOAD_HISTORICAL_DATA_FILE_PATH, "a") as lock_file:
        pass

def main():
    if not os.path.exists(LOCK_UPLOAD_HISTORICAL_DATA_FILE_PATH):
        console_log("INFO", "Uploading historical data")
        uploading_hitorical_data()
        console_log("INFO", "Building historical data")
        build_historical_data()
        console_log("INFO", "Finished building historical data")
    else:
        console_log("WARNING", "Skipping to process historical data")


if __name__ == "__main__":
    main()