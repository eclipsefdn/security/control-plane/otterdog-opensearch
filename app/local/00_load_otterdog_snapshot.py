# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************


import json
import os
import hashlib
from datetime import datetime
from etl_cli.opensearch_cli.cli import OpenSearchETL
from etl_cli.opensearch_cli.cli import console_log



### TO-DO Converting this script in a class where load_data could be decorated

OS_INDEX_NAME = 'otterdog-snapshot'
OS_HOSTNAME = "opensearch-node1"


### TO-DO Fixing problem with cardinality count

DATA_PATH="/output/otterdog"


class OpenSearchETL_snpashot(OpenSearchETL):

    def load_data(self,json_file_path: str):
        # Load JSON data from file
        timestamp = datetime.now()
        console_log("INFO",f"Processing file {json_file_path}")
        branch_patterns = lambda y: list(map( lambda x: x['pattern'],  y['branch_protection_rules']))
        with open(json_file_path, "r") as json_file:
            json_data = json.load(json_file)
            orgs_data = {'_'.join(['org',k]):v for k,v in json_data.items() if k not in ['repositories','@timestamp']} 
            orgs_data['org_github_hash'] =  hashlib.sha256(orgs_data['org_github_id'].encode()).hexdigest()
            orgs_data['@timestamp'] = f"{timestamp.strftime('%Y-%m-%dT%H:%M:%S')[:-3]}"
            ### TRANSFORMATION SECTION
            ## unwind repositories list
            for d in json_data['repositories']:
                ### Adding github org data to a repository
                d.update(orgs_data)
                d['default_branch_protection'] = 'protected' if d['default_branch'] in branch_patterns(d) else 'not_protected'
                ### Defining a document index as github_id + _ + repo.name
                yield {'_index': f'{self.os_index_name}', '_id':f"{json_data['github_id']}_{d['name']}", '_source': d}

    def updating_mapping_propperties(self):
        try:
            properties = self.os_client.indices.get(self.os_index_name)
            properties[self.os_index_name]['mappings']['properties']['org_github_id']['fielddata']=True
            properties[self.os_index_name]['mappings']['properties']['org_github_hash']['fielddata']=True
            self.os_client.indices.put_mapping(index=self.os_index_name,body=properties[self.os_index_name]['mappings'])
            console_log("INFO", f"Mapping propperties updated")
        except Exception as e:
            console_log("ERROR", f"An expected error happened when {self.os_index_name} as updated")
            console_log("ERROR",e)


def main():
    cli = OpenSearchETL_snpashot(os_hostname=OS_HOSTNAME, os_index_name=OS_INDEX_NAME)
    # Deleting index to prevent issues with all documents
    if cli.os_client.indices.exists(OS_INDEX_NAME):
        cli.os_client.indices.delete(index=OS_INDEX_NAME)
    
    for filename in os.listdir(DATA_PATH):
        if filename.endswith(".json"):
            file_path = os.path.join(DATA_PATH,filename)
            cli.insert_json_file(json_file_path=file_path)
            console_log("INFO",f"Processed file: {filename}")
    cli.updating_mapping_propperties()
    
   


if __name__ == "__main__":
    main()