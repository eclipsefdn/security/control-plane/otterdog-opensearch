# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************


import json
import os
import sys
from etl_cli.opensearch_cli.cli import OpenSearchETL
from etl_cli.opensearch_cli.cli import console_log


OS_INDEX_SNAPSHOT = 'otterdog-snapshot'
OS_INDEX_KPIS = 'otterdog-kpis'
OS_HOSTNAME = "opensearch-node1"


DATA_PATH="/output/historical"

LOCK_UPLOAD_HISTORICAL_DATA_FILE_PATH='/var/local/upload_historical_data.locl'

def load_variables():
    working_dir=os.path.dirname(os.path.abspath(__file__))
    config_file = f"{working_dir}/.config_{os.path.basename(__file__).split('.py')[0]}.json"
    try:
        with open(config_file, 'r') as config_data:
            console_log("INFO", f"Loading configuration file: {config_file}")
            global_variables = json.load(config_data)
            ## parsing files
            ## TO-DO parshing as well as CSV files
            for k,v in global_variables.items():
                if isinstance(v, dict):
                    if 'json' in v.values():
                        try:
                            console_log("INFO", f"Parsing varible {k}, definition file type {v['file_type']} getting definition from: {v['file_definition_path']}")
                            file_definition=f"{working_dir}/{v['file_definition_path']}"
                            with open(file_definition, 'r') as data:
                                global_variables[k]=json.load(data)
                        except FileNotFoundError:
                            console_log("ERROR",f"file: {file_definition} doesn't exists.")
        globals().update(global_variables)
    except json.JSONDecodeError as e:
        console_log("ERROR", f"file: {config_file} is not a valid JSON format. Error {e}")
        sys.exit(1)
    except FileNotFoundError:
        console_log("ERROR",f"file: {config_file} doesn't exists. Please check the file has the right format or follow name convetion .config_PYTHON_SCRIPT_NAME.json")
        sys.exit(1)



def main():
    cli = OpenSearchETL(os_hostname=OS_HOSTNAME, os_index_name=OS_INDEX_SNAPSHOT)
    load_variables()
    console_log("INFO", f"{QUERY_KPIS}")
    kpis = cli.query(QUERY_KPIS)['aggregations']
    console_log("INFO", f"{QUERY_SNAPSHOT_TIMESTAMP}")
    timestamp_dict = cli.query(QUERY_SNAPSHOT_TIMESTAMP)['aggregations']['SNAPSHOT_TIMESTAMP']['buckets'][0]
    console_log("INFO", f"timestamp_dict = {timestamp_dict}")
    result = {
        "@timestamp": timestamp_dict["key_as_string"],
        "total_github_id": kpis["total_orgs"]["value"],
        "total_repositories": kpis["total_repositories"]["value"],
        "default_branch_protection": dict([tuple(v.values()) for v in kpis["default_branch_protection"]["buckets"]]),
        "secret_scanning": dict([tuple(v.values()) for v in kpis["secret_scanning"]["buckets"]]) 
    }
    console_log("INFO", f"result = {result}")
    result["default_branch_protection"].update( { f'perc_{k}': round(v/sum(result['default_branch_protection'].values()),4) for k,v in result['default_branch_protection'].items() })
    result["secret_scanning"].update( { f'perc_{k}': round(v/sum(result['secret_scanning'].values()),4) for k,v in result['secret_scanning'].items() })
    console_log("INFO", f"Computing % for KPIs doc: {result}")
    cli_upload = OpenSearchETL(os_hostname=OS_HOSTNAME, os_index_name=OS_INDEX_KPIS)
    cli_upload.load_doc(result,timestamp_dict["key"])    


if __name__ == "__main__":
    main()