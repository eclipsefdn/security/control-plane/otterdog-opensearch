# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************


import json
import urllib.parse
import os
from datetime import datetime
from etl_cli.opensearch_cli.cli import OpenSearchETL
from etl_cli.opensearch_cli.cli import console_log


### TO-DO Converting this script in a class where load_data could be decorated

OS_INDEX_NAME = 'otterdog-commits'
OS_HOSTNAME = "opensearch-node1"


DATA_PATH="/output/otterdog"



class OpenSearchETL_commit(OpenSearchETL):
    def load_data(self,json_file_path:str):
        # Load JSON data from file
        timestamp = datetime.now()
        print(f"{timestamp.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]} Processing file {json_file_path}")
        branch_patterns = lambda y: list(map( lambda x: x['pattern'],  y['branch_protection_rules']))
        with open(json_file_path, "r") as json_file:

            json_data = json.load(json_file)
            ### TRANSFORMATION SECTION
            ## unwind repositories list
            for d in json_data['repositories']:
                ### Adding github org data to a repository
                console_log("INFO",f"Analysing default_branch_protection for repo: {d['name']} from github_id: {json_data['github_id']}")
                d['default_branch_protection'] = 'protected' if d['default_branch'] in branch_patterns(d) else 'not_protected'
                console_log("INFO",f"State default_branch_protection = {d['default_branch_protection']} for repo: {d['name']} from github_id: {json_data['github_id']}")
                ### Defining a document index as github_id + _ + repo.name
            yield {'_index': f'{self.os_index_name}', '_id':f"{json_data['sha_commit']}_{json_data['github_id']}", '_source': json_data}



    def updating_mapping_propperties(self):
        try:
            properties = self.os_client.indices.get(self.os_index_name)
            properties[self.os_index_name]['mappings']['properties']['repositories']['properties']['default_branch_protection']['fielddata']=True
            properties[self.os_index_name]['mappings']['properties']['github_id']['fielddata']=True
            self.os_client.indices.put_mapping(index=self.os_index_name,body=properties[self.os_index_name]['mappings'])
            console_log("INFO", f"Mapping propperties updated")
        except Exception as e:
            console_log("ERROR", f"An expected error happened when {self.os_index_name} as updated")
            console_log("ERROR",e)



def main():
    cli = OpenSearchETL_commit(os_hostname=OS_HOSTNAME, os_index_name=OS_INDEX_NAME)
    for filename in os.listdir(DATA_PATH):
        if filename.endswith(".json"):
            file_path = os.path.join(DATA_PATH,filename)
            cli.insert_json_file(file_path)
            console_log("INFO",f"Processed file: {filename}")
    cli.updating_mapping_propperties()


if __name__ == "__main__":
    main()