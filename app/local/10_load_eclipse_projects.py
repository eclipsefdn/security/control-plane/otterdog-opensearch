# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************


import json
import os
import hashlib
from datetime import datetime
from etl_cli.opensearch_cli.cli import OpenSearchETL
from etl_cli.opensearch_cli.cli import console_log
from etl_cli.eclipse import EclipseAPI
from opensearchpy import helpers
from itertools import chain

### TO-DO Converting this script in a class where load_data could be decorated

OS_INDEX_NAME = 'eclipse-projects'
OS_HOSTNAME = "opensearch-node1"

class OpenSearchETL_eclipse_projects(OpenSearchETL):
    def load_data(self):
        eclipse_projects = EclipseAPI()
        for ep in eclipse_projects.get_projects():
            console_log("INFO", f"Uploading project {ep['project_id']}")
            ep['eclipse_project_hash'] =  hashlib.sha256(ep['project_id'].encode()).hexdigest()
            if len(ep['github_repos']) > 0:
                ep['repo_hosted'] = 'github'
            if len(ep['gerrit_repos']) > 0:
                ep['repo_hosted'] = 'gerrit'
            if len(ep['website_repo']) > 0:
                if 'gitlab' in '#'.join(list(chain.from_iterable( [ i for i in chain.from_iterable([ item.items() for item in ep['website_repo']])]))):
                    ep['repo_hosted'] = 'gitlab'
                else:
                    ep['repo_hosted'] = 'other'
            yield {'_index': f'{self.os_index_name}', '_id':f"{ep['eclipse_project_hash']}", '_source': ep}

    def insert_json_file(self):
        if self.os_client:
            console_log("INFO",f"Connected to server: {self.os_hostanme}")
            try:
                helpers.bulk(self.os_client, self.load_data())
            except Exception as ex:
                console_log("ERROR",f"connecting to {self.service_uri} msg {ex}")

    def updating_mapping_propperties(self):
        try:
            properties = self.os_client.indices.get(self.os_index_name)
            properties[self.os_index_name]['mappings']['properties']['eclipse_project_hash']['fielddata']=True
            properties[self.os_index_name]['mappings']['properties']['repo_hosted']['fielddata']=True
            self.os_client.indices.put_mapping(index=self.os_index_name,body=properties[self.os_index_name]['mappings'])
            console_log("INFO", f"Mapping propperties updated")
        except Exception as e:
            console_log("ERROR", f"An expected error happened when {self.os_index_name} as updated")
            console_log("ERROR",e)


def main():
    cli = OpenSearchETL_eclipse_projects(os_hostname=OS_HOSTNAME,
                                         os_index_name=OS_INDEX_NAME)
    console_log("INFO", f"Inserting eclipse projects")
    cli.insert_json_file()
    cli.updating_mapping_propperties()

if __name__ == '__main__':
    main()