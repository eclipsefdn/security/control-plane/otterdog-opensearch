#!/usr/bin/env bash
# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************

set -euo pipefail

check_dependencies() {
  local missing_dependencies=()

  # Check for jsonnet
  if ! command -v jsonnet &> /dev/null; then
    missing_dependencies+=("jsonnet")
  fi

  # Check for jb
  if ! command -v jb &> /dev/null; then
    missing_dependencies+=("jb")
  fi

  # Check for gh
  if ! command -v jb &> /dev/null; then
    missing_dependencies+=("gh")
  fi


  if [ ${#missing_dependencies[@]} -eq 0 ]; then
    console_log INFO "All required dependencies are installed."
  else
    console_log ERROR "The following dependencies are missing:"
    for dep in "${missing_dependencies[@]}"; do
      echo "  $dep"
    done
  fi

  # Checking directories
  if [ ! -d "$DATA_PATH" ]
  then
    mkdir -p "${DATA_PATH}"
    echo "Created directory: ${DATA_PATH}"
  fi

  if [ ! -d "${OUTPUT_PATH}" ]
  then
    mkdir -p "${OUTPUT_PATH}"
    echo "Created directory: ${OUTPUT_PATH}"
  fi
}


outpath_api(){
    local output_path_api=${OUTPUT_PATH}/$1

    if [ ! -d "${output_path_api}" ]
    then
        echo "$(date '+%Y-%m-%d %H:%M:%S.%N') INFO: Creating API OUT PTATH: ${output_path_api}"
        mkdir -p "${output_path_api}"
    fi
}

console_log(){
  local type_msg=$1 ### INFO, WARNING, ETC
  local msg=$2
  local timestamp
  timestamp=$(date "+%Y-%m-%d %H:%M:%S.%N")
  echo "${timestamp} ${type_msg}: ${msg}"
}