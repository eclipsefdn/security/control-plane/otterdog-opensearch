#!/usr/bin/env bash
# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************

set -euo pipefail

# shellcheck source=/dev/null
source /scripts/helpers.sh


hostname=opensearch-dashboards
### Checking if dashboard is ready
wait-for-dashboard ${hostname}

### Ingesting all opensearch objects
uploading_opensource_objects ${hostname}