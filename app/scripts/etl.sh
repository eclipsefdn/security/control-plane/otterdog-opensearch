#!/usr/bin/env bash
# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************

set -euo pipefail

# An orchestrator script to extract and transform otterdog jsonnet

# shellcheck source=/dev/null
source /scripts/helpers.sh

echo "Downloading otterdog.json from https://github.com/EclipseFdn/otterdog-configs"
download_otterdog_configs


console_log INFO 'Extracting OTTERDOG JSONNET and Transforming into JSON'
extract_jsonnet_transform_json

console_log INFO 'Loading data into OpenSearch'
load_etl_launcher