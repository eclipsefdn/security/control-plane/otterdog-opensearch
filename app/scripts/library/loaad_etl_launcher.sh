#!/usr/bin/env bash
# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************

set -euo pipefail

load_etl_launcher(){
    for pyscripts in /app/*/*.py
    do
        echo "Executing python script: ${pyscripts}"
        echo "#####################################"
        python "${pyscripts}"
        sleep 2 ### Adding a delay to enable indexing documents already uploaded
    done
}