#!/usr/bin/env bash
# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************

set -euo pipefail

### Checking dashboard services are up
uploading_opensource_objects() {
    local hostname=${1}
    local port=${2:-5601}
    local url=http://${hostname}:${port}/api/saved_objects/_import
    if [ -z "${hostname}" ]
    then
        console_log ERROR "hostname parameter is empty"
        return 1
    fi

    console_log INFO "Service is up and running!. Ingesting Objects using API ${url}"
    # Adding all opensearch objects
    curl -XPOST -u "${OS_USER}":"${OS_PASS}" "${url}" -H "osd-xsrf: true" --form file=@"/etc/dashboard/opensearch/otterdog_all_dashboard_objects.ndjson"
}


wait-for-dashboard(){
    local hostname=${1}
    local port=${2:-5601}
    local url=http://${hostname}:${port}/api/reporting/stats 
    local is_up=0
    max_retries=${2:-10}
    retry_interval=${3:-10} # in seconds

    if [ -z "${hostname}" ]
    then
        console_log ERROR "hostname parameter is empty ${url}"
        return 1
    fi

    console_log INFO "Checking out if url ${url} is available for max retries: ${max_retries} and interval of ${retry_interval} seconds"
    for ((i = 1; i <= max_retries; i++)); do

        set +e # to prevent exit due to hostname pensearch-dashboards is no reachable
        response=$(curl -s -o /dev/null -w "%{http_code}" "$url")
        set -e
 
        if [ "$response" -eq 200 ]; then
            is_up=1
            break
        else
            console_log INFO "Retrying in $retry_interval seconds (Attempt $i of $max_retries)..., url: ${url}"
            sleep "$retry_interval"
        fi
    done
    if [ $is_up -gt 0 ]
    then
        console_log INFO "Dasboard up and running response received from $url"
    else
        console_log ERROR "Max retries reached, ${max_retries}. Dashboard is down"
        return 1
    fi
}