#!/usr/bin/env bash
# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************

set -euo pipefail

extract_jsonnet_transform_json(){
    local otterdog_file_path=${DATA_PATH}/otterdog.json
    ### TO-DO: In production subsitute [:2][] by []
    #local arr_github_id=($(jq '.organizations[].github_id' ${otterdog_file_path} ))
    local arr_github_id
    mapfile -t arr_github_id < <(jq '.organizations[].github_id' "${otterdog_file_path}" )
    local output_name=otterdog

    ### Preparing output
    outpath_api ${output_name}

    for g in "${arr_github_id[@]}"
    do
        local gh_id=${g//\"/}
        console_log INFO "${gh_id} -- processing"

        ### Creating a temporary directory
        temp_dir=$(mktemp -d -t "otterdog-${g//\"/}-XXXXXXXXXX")
        cd "${temp_dir}" || exit
        jb init
        jb install "github.com/${gh_id}/.eclipsefdn/otterdog@main"

        ## Getting commit and timestamp
        sha_commit=$(get_jsonnet_commit_id "${temp_dir}")
        commit_timestamp=$(get_commit_timestamp "${sha_commit}" "${gh_id}" .eclipsefdn) 

        cp "vendor/otterdog/${gh_id}.jsonnet" vendor/otterdog-defaults/
        cd "${temp_dir}/vendor/otterdog-defaults/" || exit
        sed -i 's/vendor\/otterdog-defaults\///g' "${gh_id}.jsonnet"
        jsonnet "${gh_id}.jsonnet" | jq ". + { \"@timestamp\": \"${commit_timestamp}\", \"sha_commit\": \"${sha_commit}\" }" > "${OUTPUT_PATH}/${output_name}/${gh_id}.json"

        ### Cleaning up temporary directory
        rm -rf "${temp_dir}"

    done
}

get_jsonnet_commit_id() {
    local temp_dir=$1
    if [ -z "$temp_dir" ] || [ ! -e "${temp_dir}/jsonnetfile.lock.json" ]; then
        console_log "ERROR!!!" "Parameter is empty or file ${temp_dir}/jsonnetfile.lock.json doesn't exist" >&2
        return 100
    else
        local version
        version=$(jq -r '.dependencies[] | select(.source.git.subdir == "otterdog") | .version' "${temp_dir}/jsonnetfile.lock.json")
        echo "$version"
    fi
}