#!/usr/bin/env bash
# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************

set -euo pipefail

get_commit_timestamp(){
    local sha_commit_id=$1
    local github_id=$2
    local github_repo=$3
    if [ "$#" -eq 3 ]
    then
        timestamp=$(gh api \
            -H "Accept: application/vnd.github+json" \
            -H "X-GitHub-Api-Version: 2022-11-28" \
            "/repos/${github_id}/${github_repo}/commits/${sha_commit_id}" | jq '.commit.author.date' | sed 's/"//g')
        if [ -z "$timestamp" ] || [ "$timestamp" == "null" ]
        then
            console_log "ERROR!!!" "Unexpected error happend executing gh client. Possible issue 'gh: Bad credentials (HTTP 401)'" >&2
            return 200
        else
            echo "$timestamp"
        fi
    else
        console_log "ERROR!!!" "sha_commit_id or github_id or github_repo are empty"
        return 200        
    fi
}

extract_github_api(){
    ### PARAMETERS
    local output_name=$1 #### Name OUTPUT_PATH sub-directory
    local api_path=$2 ### API URL
    local api_version=${3:-2022-11-28}

    ### LOCAL VARIABLES
    local otterdog_file_path=${DATA_PATH}/otterdog.json 
    local output_path="${OUTPUT_PATH}/${output_name}"
    
    ### Preparing API OUTPUT
    outpath_api "${output_name}"

    ### TO-DO: In production subsitute [:2][] by []
    #local arr_github_id=($(jq '.organizations[].github_id' ${otterdog_file_path} ))
    local arr_github_id
    mapfile -t arr_github_id < <(jq '.organizations[].github_id' "${otterdog_file_path}" )
    for g in "${arr_github_id[@]}"
    do
        local gh_id=${g//\"/}
        console_log INFO "${gh_id} -- processing code scanning alertas for ${output_name} from ${GITHUB_ENDPOINT}/${gh_id}/${api_path}"
        ### CALLING API
        gh api --paginate \
        -H "Accept: application/vnd.github+json" \
        -H "X-GitHub-Api-Version: ${api_version}" \
        "/orgs/${gh_id}/${api_path} > ${output_path}/${output_name}_${gh_id}.json"
        ### A delay to avoid rate limits
        ## https://docs.github.com/en/rest/guides/best-practices-for-using-the-rest-api?apiVersion=2022-11-28#dealing-with-rate-limits
        ## https://cli.github.com/manual/gh_api
        sleep 0.5
    done
}


extract_github_code_scanning_alerts(){
    extract_github_api code-scanning  "code-scanning/alerts"
}

extract_github_dependabot_alerts(){
    extract_github_api dependabot  "dependabot/alerts"
}