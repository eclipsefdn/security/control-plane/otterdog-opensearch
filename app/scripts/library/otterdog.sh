#!/usr/bin/env bash
# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************

set -euo pipefail

download_otterdog_configs(){
    curl --silent https://raw.githubusercontent.com/EclipseFdn/otterdog-configs/main/otterdog.json --output "${DATA_PATH}"/otterdog.json
    echo "Downloaded: ${DATA_PATH}/otterdog.json"
}