#!/usr/bin/env bash
# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************

set -euo pipefail

# shellcheck source=/dev/null
source /scripts/helpers.sh

### Wait-for-dashboard to ingest All OpenSearch Objects
./wait-for-dashboard.sh

## TO-DO convert this loop into schedule job

while true 
do
    ## This script launch all bash and python scripts located on /app to load data upon database engine
    ## Loop to execute every ${ETL_SLEEPTIME}
    ./etl.sh
    sleep "${ETL_SLEEPTIME}"
done