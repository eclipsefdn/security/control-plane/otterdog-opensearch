#!/usr/bin/env bash
# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************

set -euo pipefail

SCRIPT_PATH="$( cd -- "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 || exit ; pwd -P )"

LIBRARY_PATH=${SCRIPT_PATH}/library
DATA_PATH="$(dirname "${SCRIPT_PATH}")/data"
OUTPUT_PATH="$(dirname "${SCRIPT_PATH}")/output"

export SCRIPT_PATH
export DATA_PATH
export OUTPUT_PATH

# Sourcing global variables

# shellcheck source=/dev/null
source "${SCRIPT_PATH}"/GLOBAL.sh


if [ -d "$LIBRARY_PATH" ]
then
    for script in "${LIBRARY_PATH}/"*.sh
    do
        if [ -f "${script}" ]
        then
            # shellcheck source=/dev/null
            source "${script}"
        fi
    done
else
    echo "ERROR!!!: Folder ${LIBRARY_PATH} not found"
    exit 1
fi


check_dependencies
