# Changelog

## [0.1.0-dev] - unreleased

### Added

* A dashboard to monitor otterdog activity
* A dashboard to monitor opensearch index activity
* Scripts to pull, transform jsonnet load the information into three index snapshot, commits and kpis
* Documented the solution architecture

## [0.1.2-dev] - 2023-11-14

### Fixed
- Improved security by using Bash Strict Mode #15
- Improved security by testing and fixing issues using shellchecks #14


## Removed
- Removed grafana dockerfile #13
- Removed directory scripts and moved to app directory #9
- Removed directory data and moved to app directory #7


## [0.1.1-dev] - 2023-11-08

### Added

* Realising a developer version