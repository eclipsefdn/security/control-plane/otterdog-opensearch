#!/bin/bash


curl -XGET "https://opensearch-node1:9200/otterdog-snapshot/_search" -H 'Content-Type: application/json' -d'
{
  "size": 0,
  "_source": ["org_github_id"], 
  "aggs": {
    "total_orgs": {
      "cardinality": {
        "field": "org_github_id",
        "precision_threshold": 40000
      }
    }, "total_repositories":{
      "value_count": {
        "field": "name.keyword"
      }
    }, "default_branch_protection": {
      "terms": {
        "field": "default_branch_protection.keyword",
        "size": 2
      }
    }, "secret_scanning": {
      "terms": {
        "field": "secret_scanning.keyword",
        "size": 2
      }
    }
  }
}'