#!/bin/bash

curl -XGET "https://opensearch-node1:9200/otterdog/_search" -H 'Content-Type: application/json' -d'
{
  "sort": [
    {
      "github_id.keyword": {
        "order": "desc"
      }
    }
  ], 
  "_source": ["repositories.branch_protection_rules","repositories.name"], 
  "query": {
    "match_all": {}
  }
  , "script_fields": {
    "repositories": {
      "script": "params['\''_source'\'']"
    }
  }
}'