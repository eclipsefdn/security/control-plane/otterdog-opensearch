#!/bin/bash

####
# Aggregated branch_protection_rules.pattern including missed

curl -XGET "https://opensearch-node1:9200/otterdog-github-repositories/_search" -H 'Content-Type: application/json' -d'
{
  "aggs": {
    "2": {
      "terms": {
        "field": "branch_protection_rules.pattern.keyword",
        "order": {
          "_count": "desc"
        },
        "missing": "not protected",
        "size": 5
      }
    }
  },
  "size": 0,
  
  "query": {
    "bool": {
      "must": [],
      "filter": [
        {
          "match_all": {}
        }
      ]
    }
  }
}'