#!/bin/bash

curl -XGET "https://opensearch-node1:9200/otterdog/_search" -H 'Content-Type: application/json' -d'
{
  "query": {
    "bool": {
      "must_not": [
        {
          "nested": {
            "path": "secrets",
            "query": {
              "exists": {
                "field": "secrets"
              }
            }
          }
        }
      ]
    }
  }
}'