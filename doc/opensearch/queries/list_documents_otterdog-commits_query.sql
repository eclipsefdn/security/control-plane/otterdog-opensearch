SELECT count(repositories.name) AS TOTAL_REPOS, github_id, @timestamp, sha_commit
FROM otterdog-commits
where  @timestamp > timestamp('2023-10-25 00:00:00') and @timestamp <= timestamp('2023-10-26 00:00:00')
GROUP BY github_id, @timestamp, sha_commit
ORDER BY @timestamp DESC