# HOW-TO

## HOW-TO import OpenSearch Objects
* After log in into OpenSearch Dashboard, Go to Menu (3 horizontal lines on the upper left coner) > Dashboard Management (from Management Section) > Saved Objects
* Click import
* Click options Check for existing objects and Automatically overwrite conflicts
* Click Import icon above and select file from repo [dashboard/opensearch/otterdog_all_dashboard_objects.ndjson](../dashboard/opensearch/otterdog_all_dashboard_objects.ndjson)
* Next, click the button Import on the lower right conner.
* Check no issues and click the button Done