# ARCHITECTURE

The architecture is based on five continers with different roles below:

* ETL container: execute scripts to extract, transform and load information into an OpenSearch Node Container
* OpenSearch Node container: Storing all documents as database and for simplicty and data volume only a node is deployed
* OpenSearch Dashbard container: A GUI to manage and query OpenSearch databases as well as dashboard for KPIs defined
* Grafana container: A dashboard with to monitor KPIs defined

```html
  +------------------------------------------------------+                                                                                                                      
  |                                                      |                                                                                                                      
  |  github_org/.eclipsefdn/otterdog/github_org.jsonnet  |                                                                                                                      
  |                                                      |                                                                                                                      
  +---------------------------|--------------------------+                                                                                                                      
                              |                                                                                                                                                 
                              |                                                                                                                                                 
                              |                                                                                                                                                 
                              |                                                                                                                                                 
                              |                                                                                                                                                 
                              |                                                                                                                                                 
                              |                                                                                                                                                 
                              |                                                                                                                                                 
+-----------------------------|----------------------------------------------------------------+     +---------------------------------------+                                  
|  +----------------------------------------+        +-------------------------------------+   |     |                                       |                                  
|  |               +---------------------+  |        |          PYTHON SCRIPTS             |   |     |    +------------------------------+   |     +--------------------------+ 
|  |               |      download       |  |        |                                     |   |     |    |                              |   |     |                          | 
|  |               |  otterdog configs   |  |        |   +-----------------------------+   |   | ----------  otterdog-snapshot (index)   |   |     |   OPENSEARCH-CONTAINER   | 
|  |               +---------------------+  |        |   | 00_load_otterdog_snapshot   ---------/    |    |                              |   |     |        CONTAINER         | 
|  |               +---------------------+  |        |   +-----------------------------+   |   |     |    +------------------------------+   |     |                          | 
|  |     ETL       | extract & transform |  |        |   +-----------------------------+   |   |     |    +------------------------------+   |     +--------------------------+ 
|  |  BASH SCRIPT  |  jsonnet -> json    |  |        |   | 70_load_otterdog_commits    ---------\    |    |                              |   |                                  
|  |               +---------------------+  |      ---   +-----------------------------+   |   | ----------   otterdog-commits (index)   |   |                                  
|  |               +---------------------+  |-----/  |   +-----------------------------+   |---------/    |                              |   |                                  
|  |               |      load etl       |--/        |   | 80_load_otterdog_historica  ----/   |     |    +------------------------------+   |                                  
|  |               |      launcher       |  |        |   |       (temp for POC)        |   \-----    |    +------------------------------+   |                                  
|  |               +---------------------+  |        |   +-----------------------------+   |   | \-----   |                              |   |     +--------------------------+ 
|  +----------------------------------------+        |   +-----------------------------+   |   | ----------    otterdog-kpis (index)     |   |     |                          | 
|                                                    |   |   90_load_otterdog_kpis     ---------/    |    |                              |   |     |         GRAFANA          | 
|                                                    |   +-----------------------------+   |   |     |    +------------------------------+   |     |        CONTAINER         | 
|                                                    +-------------------------------------+   |     |                                       |     |                          | 
|                                                                                              |     |                                       |     +--------------------------+ 
|                                                                                              |     |                                       |                                  
|                                        ETL                                                   |     |           OPENSEARCH-NODE-1           |                                  
|                                     CONTAINER                                                |     |               CONTAINER               |                                  
+----------------------------------------------------------------------------------------------+     +---------------------------------------+                                  

```

## INGESTION PROCESS
* The ingestion process is based on the execution of bash, python scripts and it's executed every 30 minutes
* There are two type of bash scripts:
  * Bash scripts to download and transform a jsonnet from a Github Organisation into a json file 
  * A bash script to lauch the python scripts.
* The python scripts upload and some times transform a json document above into an OpenSearch index.


## INDICES

### otterdog-snapshot
* _id: composed by github_id and repo.name
* @timestamp: set up at time is uploaded
* Description: Storing documents after being transformed from jsonnet into a json document. Then transformed into a document for each repostiry with Github properties

### otterdog-commit
* _id: jsonnet commit
* @timestamp: jsonnet commit timestamp got querying GH when jsonnet is transformed
* Description: Storing documents after transforming a jsonnet file to json

### otterdog-kpis
* _id: a version epoch of @timesampt
* @timestamp: timestamp at time when is generated the document
* Description: Storing documents with KPIS:
  * num of repos with default_branch_protection protected/not_protected
  * % of repos wtih default_branch_protection protected/not_protected
  * num of repos with secret_scanning_enabled/disabled
  * % of repos with secret_scanning_enabled/disabled
  * total_repositories
  * total_orgs
