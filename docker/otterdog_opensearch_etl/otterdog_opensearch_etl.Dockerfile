# *******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the MIT License
# which is available at https://spdx.org/licenses/MIT.html
# SPDX-License-Identifier: MIT
# *******************************************************************************


FROM gcc:9.5.0-bullseye as build_jsonnet
WORKDIR /app
RUN git clone https://github.com/google/jsonnet.git /app
RUN make

FROM golang:latest as build_jsonnet_bundler
RUN go install -a github.com/jsonnet-bundler/jsonnet-bundler/cmd/jb@latest

FROM python:3.10-slim

# Set the user ID and group ID to match the host user's IDs
ARG USER_ID
ARG GROUP_ID
ARG GH_TOKEN
ARG GITHUB_TOKEN
ARG ETL_SLEEPTIME=1800


COPY --from=build_jsonnet /app/jsonnet /bin
COPY --from=build_jsonnet /app/jsonnetfmt /bin
COPY --from=build_jsonnet_bundler /go/bin/jb /bin

RUN apt-get update && apt-get install -y --no-install-recommends curl jq git iputils-ping wget gnupg coreutils openssl

RUN curl -fsSL https://cli.github.com/packages/githubcli-archive-keyring.gpg |  dd of=/usr/share/keyrings/githubcli-archive-keyring.gpg \
&&  chmod go+r /usr/share/keyrings/githubcli-archive-keyring.gpg \
&& echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/githubcli-archive-keyring.gpg] https://cli.github.com/packages stable main" |  tee /etc/apt/sources.list.d/github-cli.list > /dev/null \
&&  apt-get update \
&&  apt-get install gh -y

# Create a user and group in the container with matching IDs
RUN groupadd -g $GROUP_ID hostgroup && \
    useradd -u $USER_ID -g $GROUP_ID -ms /bin/bash hostuser


RUN apt-get clean && \
  rm -rf /var/lib/apt/lists/*

COPY dist/ /tmp/dist/
COPY requirements.txt /tmp/requirements.txt


RUN pip install -r /tmp/requirements.txt

USER hostuser
WORKDIR /scripts

CMD ["/scripts/entrypoint.sh"]
