# Otterdog Opensearch

This is a POC to build a OpenSearch database, hosted locally and desiging a dashboard based on a list of KPIs below:
* Total of Github Organisations
* Total of Repositories
* Total and Percentage of repositories with Secret Scanning enabled
* Total and Percentage of repositories where default branch has some protection rules.

The database is based on data got from the jsonnet applied to those Github Eclipse Organization where otterdog have been installed

This project create the containers below:
* otterdog_etl: A container with scripts to extract, transforms and load each jsonet
* opensearch-node1: A container with scripts to OpenSearch already installed
* opensearch-dashboard: A container with OpenSearch Dashboard

## Out of Scope, limitations and issues

### Out of Scope
* Designing a OpenSearch Infraestructure
* Designing a full ETL process as well as designing an orchestration process to ingesting data

### Limitations
* This POC is limitated to have only one opensearch node.

### Issues:
* Currently, the script [80_load_otterdog_historical.py](./app/local/80_load_otterdog_historical.py) should run twice to rebuild the historical data based on data placed on directory .historical by executing first ```make shell_etl```, then ```python /app/local/80_load_otterdog_historical.py```

## Requirements
* GitHub token with permissions
  * repo full access
  * workflow
  * admin:org/read:org
  * admin:org_hook
  * audit:log/read:audit_log (only)
  * project/read:project (only)
* ggshield (optional and recommended for developers)
* docker && docker-compose
* git hooks from [eclipsefdn/security/scripts/git_hooks](https://gitlab.eclipse.org/eclipsefdn/security/scripts/-/tree/main/git_hooks?ref_type=heads)

## Deploying containers
* After cloning repositorie execute the snippet below
```bash
make build
make up
```
* To access to OpenSearch Dashboard (Linux + Google Chrome)
```bash
make run_dashboard
make run_grafana
```
* If you want to re-deploy the environment, it's recommended to clean all persistant data using ```make clean_compose``` (WARNING: this will download all data from scratch)
## Dashboard access
### OpenSearch Dashboard
* Please, execute ```make run_dashboard``` (WARNING: it only works on Linux with Google Chrome) or open browser and go to URL, http://localhost:5601
* Credentials admin/admin (TO-DO: customizing credentials)
* Import all OpenSearch objects following the steps described below, [HOW-TO import OpenSearch Objects](./doc/HOWTO.md#how-to-import-opensearch-objects)

### Grafana
* Please, execute ```make run_grafana``` (WARNING: it only works on Linux with Google Chrome) or open browser and go to URL, http://localhost:3000
* Credentials admin/test (TO-DO: customizing credentials)

## Monitoring Containers
All macros below use ```docker-compose logs -f```
* ETL Container ```make logs_etl```
* OpenSearch node 1 ```make logs_etl```
* All containers ```make logs```

## Architecture
Please go to document [architecture.md](./doc/architecture.md)

## HOW-TO
Please go to docment [HOWTO.md](./doc/HOWTO.md)


## More Information
* [OpenSearch Get Started](https://opensearch.org/versions/opensearch-2-9-0.html)